provider "aws" {
  profile = "terraform"
  region = "ap-south-1" # Change this to your desired region
}

resource "aws_instance" "pradeep-ec2-instance" {
  ami           = "ami-0e1d06225679bc1c5"
  instance_type = "t2.micro"

  tags = {
    Name = "pradeep-ec2-instance"
  }
}
