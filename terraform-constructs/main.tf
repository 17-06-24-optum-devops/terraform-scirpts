# Configure the AWS Provider
provider "aws" {
 profile = "terraform"
 region = "ap-south-1"
}

resource "aws_iam_user" "iam_users" {
  #count = length(var.users)
  for_each = {for idx, user in var.users: idx => user}
  name = each.value
}

variable "users" {
  description = "IAM users"
  type = list(string)
  default = ["harish", "vinay", "vikas", "kishan"]
}

 output "all_arns" {
   value = values(aws_iam_user.iam_users)[*].arn
   description = "All Users ARN"
 }

 output "upper_names" {
   value = [for name in var.users : upper(name) if length(name) > 2]
   description = "upper case names"
 }

 variable "captain_names" {
   description = "A map of adjectives and nouns"
   type = map(string)
   default =  {
       india = "Virat Kohli"
       australia = "Aaron Finch"
       south-africa = "Faf du Plessis"
   }
 }

  output "print_captain_names" {
    value = [for team,name in var.captain_names: "${name} is the captain for ${team} team "]
  }

 variable "states"{
     description = "south indian states in India"
    type = list(string)
     default =  ["karnataka","tamil-nadu", "kerala", "andhra", "telangana"]
 }

 output "print-states" {
   value = <<EOF
   %{~ for name in var.states }
     ${name}
   %{~ endfor }
   EOF
 }

#resource "aws_instance" "ec2_instance" { 
    #count = 3
    #count = length(data.aws_availability_zones.mumbai.names)
    #availability_zone = data.aws_availability_zones.mumbai.names[count.index]
    #count = random_integer.numbers.result
    #ami = "ami-04b2519c83e2a7ea5" 
    ##instance_type = "t2.micro"
   # tags = {
    #    Name = "nginx-instance",
    #}
#}


#data "aws_availability_zones" "mumbai" {}

##resource "random_integer" "numbers" {
 # min = 2
 # max = 5
#}
