output "start_date" {
  value = var.start_date
}

output "aws_instance_type" {
  value = var.aws_instance_type
}

#string functions
output "uppercase_string" {
  value = upper(var.instance_type)
}

variable "input_string" {
  default = "Hello, world"
}

output "substing_example" {
  value = substr(var.input_string, 0, 5)
}

output "sum" {
  value = "${var.num1 + var.num2}"
}

