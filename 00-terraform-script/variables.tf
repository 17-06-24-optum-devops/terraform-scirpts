# a simple string variable
variable "instance_type" {
  type        = string
  default     = "t2.medium"
  description = "the type of aws instance to use"
}

variable "num1" {
  default = 10
}

variable "num2" {
  default = 5
}

# variable with validation
variable "aws_instance_type" {
  type        = string
  default     = "t2.micro"
  description = "the type of aws instance to use"

  validation {
    condition = contains(["t2.micro","t2.small", "t3.micro"], var.aws_instance_type)
    error_message = "Invalid instance type passed."
  }  
}

# variables with number
variable "instance_count" {
  description = "The number of instances to create"
  type = number
  default = 2
}

# variables with number
variable "enable_logging" {
  description = "Enable or disable logging"
  type = bool
  default = true
}

# variables with date
variable "start_date" {
  description = "The start date of the deployment"
  type = string
  default = "2024-10-10"
}

# list of string
variable "availability_zones" {
  description = "list of aws availability zones in mumbai region"
  type = list(string)
  default = [ "ap-south-1a", "ap-south-1b", "ap-south-1c" ]
}

# map
variable "tags" {
    description = "maps of tags"
    type = map(string)
    default = {
      "name" = "instance-1"
      "env" = "dev"
      "project" = "optum-dev"
    }
}

#variable with sensitive data
variable "db_password" {
  type = string
  sensitive = true
  default = "welcome"
}
