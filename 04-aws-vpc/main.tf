provider "aws" {
  profile = "terraform"  
  region = "ap-south-1"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "main-vpc"
  }
}

resource "aws_subnet" "public-a" {
  vpc_id = aws_vpc.main.id 
  cidr_block = "10.0.0.0/26"
  availability_zone = "ap-south-1a"
   tags = {
    Name = "public-subnet-a"
  }
}

resource "aws_subnet" "public-b" {
  vpc_id = aws_vpc.main.id 
  cidr_block = "10.0.0.64/26"
  availability_zone = "ap-south-1b"
   tags = {
    Name = "public-subnet-b"
  }
}

resource "aws_subnet" "private-a" {
  vpc_id = aws_vpc.main.id 
  cidr_block = "10.0.0.128/26"
  availability_zone = "ap-south-1a"
   tags = {
    Name = "private-subnet-a"
  }
}

resource "aws_subnet" "private-b" {
  vpc_id = aws_vpc.main.id 
  cidr_block = "10.0.0.192/26"
  availability_zone = "ap-south-1b"
   tags = {
    Name = "private-subnet-b"
  }
}

#internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-igw"
  }
}

#route table
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "public-route-table"
  }
}

#associate the public route table to the subnets

resource "aws_route_table_association" "subnet-a-associate" {
  subnet_id = aws_subnet.public-a.id
  route_table_id = aws_route_table.public-route-table.id
}

#associate the public route table to the subnets

resource "aws_route_table_association" "subnet-b-associate" {
  subnet_id = aws_subnet.public-b.id
  route_table_id = aws_route_table.public-route-table.id
}
