provider "aws" {
  profile = "terraform"  
  region = "ap-south-1"
}

resource "aws_sqs_queue" "example_queue" {
  name = "my-queue"
  delay_seconds = 0
  max_message_size = 262144
  message_retention_seconds = 345600
  receive_wait_time_seconds = 0
  visibility_timeout_seconds = 300  # Set the visibility timeout here
  fifo_queue = false  # Set to true for FIFO queue, false for standard queue
}

output "queue_url" {
  value = aws_sqs_queue.example_queue.id
}
