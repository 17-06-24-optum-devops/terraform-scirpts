# a simple string variable
variable "greet" {
  type        = string
  default     = "hello-world from terraform"
}

output "print" {
  value = var.greet
}