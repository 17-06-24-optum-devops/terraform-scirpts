provider "aws" {
  profile = "terraform"
  region = "ap-south-1" # Change this to your desired region
}

variable "users" {
    description = "IAM users"
    type = list(string)
    default = ["user-1", "user-2"]
}

resource "aws_iam_user" "iam_users" {
    for_each = { for idx, user in var.users : idx => user }
    name = each.value
}